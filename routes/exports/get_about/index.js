module.exports = ({
    router
}, data) => {

    router.get('/about', function (req, res, next) {
        res.render('about', {
            title: 'Pingzee Package Testing', 
            about:"This just to test the packaging of this app"
        });
    });


    return router;
}