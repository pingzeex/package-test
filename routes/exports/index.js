module.exports = ({
  _imports = {},
  _customs = {},
  _services = {},
  _controllers = {},
  _urls = {},
  _entities = {}
}, router1) => {

  /*  do not touch this file unless you want to add some new property */
  /*  export all the routes  */
  const {
    lstatSync,
    readdirSync
  } = require('fs')
  const {
    join
  } = require('path')

  const isDirectory = source => lstatSync(source).isDirectory()
  const getDirectories = source =>
    readdirSync(source).map(name => join(source, name)).filter(isDirectory)

  var express = require('express');
  var router = express.Router();
  getDirectories(join(__dirname, "/")).forEach((file) => {

    router = require(file)({
      router,
      _imports,
      _customs,
      _services,
      _controllers,
      _entities,
      _urls,
    }, {})

  });

  return router

}