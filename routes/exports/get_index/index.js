module.exports = ({
    router
}, data) => {

    router.get('/', function (req, res, next) {
        res.render('index', {
            title: 'Pingzee Package Testing'
        });
    });


    return router;
}